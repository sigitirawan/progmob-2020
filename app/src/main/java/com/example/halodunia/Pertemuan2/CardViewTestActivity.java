package com.example.halodunia.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.halodunia.Adapter.MahasiswaCardAdapter;
import com.example.halodunia.Adapter.MahasiswaRecyclerAdapter;
import com.example.halodunia.Model.Mahasiswa;
import com.example.halodunia.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Agus", "72180211", "082255667700");
        Mahasiswa m2 = new Mahasiswa("Anus", "72180212", "082255667701");
        Mahasiswa m3 = new Mahasiswa("Api", "72180213", "082255667702");
        Mahasiswa m4 = new Mahasiswa("Aus", "72180214", "082255667703");
        Mahasiswa m5 = new Mahasiswa("Ati", "72180215", "082255667704");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        rv.setAdapter(mahasiswaCardAdapter);
    }
}