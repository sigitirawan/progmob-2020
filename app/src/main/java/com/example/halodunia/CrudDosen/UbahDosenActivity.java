package com.example.halodunia.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.halodunia.Model.DefaultResult;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_dosen);
        EditText edNidnLama = (EditText)findViewById(R.id.edNidnLama);
        EditText edNamaBaru = (EditText)findViewById(R.id.edNamaBaruDosen);
        EditText edNidnBaru = (EditText)findViewById(R.id.edNidnBaruDosen);
        EditText edAlamatBaru = (EditText)findViewById(R.id.edAlamatBaruDosen);
        EditText edEmailBaru = (EditText)findViewById(R.id.edEmailBaruDosen);
        EditText edGelarBaru = (EditText)findViewById(R.id.edGelarBaruDosen);
        Button btnSimpanUbah = (Button)findViewById(R.id.btnSimpanUbahDosen);
        pd = new ProgressDialog(UbahDosenActivity.this);

        btnSimpanUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> hapus = service.delete_dosen(
                        edNidnLama.getText().toString(),
                        "72180212");

                hapus.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this,"GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefaultResult> call = service.add_dosen(
                        edNamaBaru.getText().toString(),
                        edNidnBaru.getText().toString(),
                        edAlamatBaru.getText().toString(),
                        edEmailBaru.getText().toString(),
                        edGelarBaru.getText().toString(),
                        "kosong",
                        "72180212"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(UbahDosenActivity.this, "DATA GAGAL DIUBAH", Toast.LENGTH_LONG).show();
                    }
                });


            }
        });
    }
}