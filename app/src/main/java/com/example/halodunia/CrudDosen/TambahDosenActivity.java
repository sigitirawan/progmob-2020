package com.example.halodunia.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.halodunia.Model.DefaultResult;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_dosen);

        EditText namaDosen = (EditText)findViewById(R.id.editNamaDosen);
        EditText nidnDosen = (EditText)findViewById(R.id.editNidnDosen);
        EditText alamatDosen = (EditText)findViewById(R.id.editAlamatDosen);
        EditText emailDosen = (EditText)findViewById(R.id.editEmailDosen);
        EditText gelarDosen = (EditText)findViewById(R.id.editGelarDosen);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanDosen);
        pd = new ProgressDialog(TambahDosenActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        namaDosen.getText().toString(),
                        nidnDosen.getText().toString(),
                        alamatDosen.getText().toString(),
                        emailDosen.getText().toString(),
                        gelarDosen.getText().toString(),
                        "kosong",
                        "72180212"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this, "DATA BERHASIL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(TambahDosenActivity.this, "DATA GAGAL DITAMBAHKAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}