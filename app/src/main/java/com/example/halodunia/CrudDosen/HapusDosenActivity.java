package com.example.halodunia.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.halodunia.Model.DefaultResult;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapusDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_dosen);
        EditText NidnHapus = (EditText)findViewById(R.id.editNidnHapusDosen);
        Button btnHapusDosen = (Button)findViewById(R.id.btnSimpanDelDosen);
        pd = new ProgressDialog(HapusDosenActivity.this);

        btnHapusDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dosen(NidnHapus.getText().toString(), "72180212");

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this,"BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(HapusDosenActivity.this,"GAGAL DIHAPUS", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}