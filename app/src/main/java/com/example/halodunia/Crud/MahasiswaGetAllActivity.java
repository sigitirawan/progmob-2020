package com.example.halodunia.Crud;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;


import com.example.halodunia.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.halodunia.Model.MahasiswaCRUD;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;



import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaGetAllActivity extends AppCompatActivity {
    RecyclerView rvMhs;
    MahasiswaCRUDRecyclerAdapter mhsAdapter;
    ProgressDialog pd;
    List<MahasiswaCRUD> mahasiswaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_get_all);
        rvMhs = (RecyclerView)findViewById(R.id.rvGetMhsAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MahasiswaCRUD>> call = service.getMahasiswa("72180212");

        call.enqueue(new Callback<List<MahasiswaCRUD>>() {
            @Override
            public void onResponse(Call<List<MahasiswaCRUD>> call, Response<List<MahasiswaCRUD>> response) {
                pd.dismiss();
                mahasiswaList = response.body();
                mhsAdapter = new MahasiswaCRUDRecyclerAdapter(mahasiswaList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MahasiswaGetAllActivity.this);
                rvMhs.setLayoutManager(layoutManager);
                rvMhs.setAdapter(mhsAdapter);
            }

            @Override
            public void onFailure(Call<List<MahasiswaCRUD>> call, Throwable t) {
                Toast.makeText( MahasiswaGetAllActivity.this,"Error", Toast.LENGTH_LONG).show();

            }
        });
    }
}
