package com.example.halodunia.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.halodunia.Model.DefaultResult;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaHapusActivity extends AppCompatActivity {

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_hapus);

        EditText edNim = (EditText)findViewById(R.id.editNim);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        pd = new ProgressDialog(MahasiswaHapusActivity.this);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(
                        edNim.getText().toString(),
                        "72180212"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaHapusActivity.this,"Berhasil di Hapus", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MahasiswaHapusActivity.this,MahasiswaGetAllActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaHapusActivity.this, "Gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}