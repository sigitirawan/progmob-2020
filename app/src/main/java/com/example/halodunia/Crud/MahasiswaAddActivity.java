package com.example.halodunia.Crud;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.halodunia.Adapter.MahasiswaCRUDRecyclerAdapter;
import com.example.halodunia.MainActivity;
import com.example.halodunia.Model.DefaultResult;
import com.example.halodunia.Model.Mahasiswa;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;
import com.example.halodunia.TrackerActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaAddActivity  extends AppCompatActivity {


    ProgressDialog pd;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_add);


        final EditText edNama =(EditText)findViewById(R.id.editTextNama);
        final EditText edNim =(EditText)findViewById(R.id.editTextNim);
        final EditText edAlamat =(EditText)findViewById(R.id.editTextAlamat);
        final EditText edEmail =(EditText)findViewById(R.id.editTextEmail);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMhs);
        pd = new ProgressDialog(MahasiswaAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitTwitterInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_mhs(
                        edNama.getText().toString(),
                        edNim.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "Kosongkan saja",
                        "72180212"

                );
               call.enqueue(new Callback<DefaultResult>() {
                   @Override
                   public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                       pd.dismiss();
                       Toast.makeText(MahasiswaAddActivity.this,"Data Berhasil Disimpan",Toast.LENGTH_LONG);
                       Intent intent = new Intent(MahasiswaAddActivity.this,MahasiswaGetAllActivity.class);
                       startActivity(intent);
                   }

                   @Override
                   public void onFailure(Call<DefaultResult> call, Throwable t) {
                       pd.dismiss();
                       Toast.makeText(MahasiswaAddActivity.this,"Gagal",Toast.LENGTH_LONG);
                   }
               });

            }
        });


    }

}
