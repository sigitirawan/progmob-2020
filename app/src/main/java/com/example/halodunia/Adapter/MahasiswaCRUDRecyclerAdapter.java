package com.example.halodunia.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.halodunia.Model.MahasiswaCRUD;
import com.example.halodunia.R;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<MahasiswaCRUD> mahasiswaList;

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaCRUDRecyclerAdapter(List<MahasiswaCRUD> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }
    public void setMahasiswaList(List<MahasiswaCRUD> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }
    public List<MahasiswaCRUD> getMahasiswaList() {
        return mahasiswaList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_mahasiswa,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MahasiswaCRUD m = mahasiswaList.get(position);
        holder.tvNama.setText(m.getNama());
       // holder.tvNoTelp.setText(m.getNotelp());
        holder.tvNim.setText(m.getNim());
        holder.tvAlamat.setText(m.getAlamat());
        holder.tvEmail.setText(m.getEmail());
        holder.tvFoto.setText(m.getFoto());



    }

    @Override
    public int getItemCount() { return mahasiswaList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp,tvAlamat,tvEmail,tvFoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvNim = itemView.findViewById(R.id.tvNim);
            //tvNoTelp = itemView.findViewById(R.id.tvNoTelp);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvFoto = itemView.findViewById(R.id.tvFoto);


        }
    }
}
