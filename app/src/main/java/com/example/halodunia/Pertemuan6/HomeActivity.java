package com.example.halodunia.Pertemuan6;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.halodunia.Crud.MainMhsActivity;
import com.example.halodunia.CrudDosen.MainDosenActivity;
import com.example.halodunia.CrudMatakuliah.MainMatkulActivity;
import com.example.halodunia.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Button menuMhs = (Button)findViewById(R.id.buttonMahasiswa);
        Button menuDosen = (Button)findViewById(R.id.buttonDosen);
        Button menuMatkul = (Button)findViewById(R.id.buttonMatakuliah);
        Button btnLogout = (Button)findViewById(R.id.btnLogout);

        menuMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(i);
            }
        });

        menuDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainDosenActivity.class);
                startActivity(i);
            }
        });

        menuMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MainMatkulActivity.class);
                startActivity(i);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, PrefActivity.class);
                startActivity(i);

            }
        });
    }
}