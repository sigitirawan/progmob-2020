package com.example.halodunia.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.halodunia.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        //Membaca pref is login true atau false
        isLogin = pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            btnPref.setText("Logout");
        }else{
            btnPref.setText("Login");
        }
        //Pengisian pref
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLogin.equals("0")){
                    editor.putString("isLogin","1");
                    Intent intent = new Intent(PrefActivity.this, HomeActivity.class);
                    startActivity(intent);
                }else{
                    editor.putString("isLogin","0");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });
        }
}