package com.example.halodunia.CrudMatakuliah;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.halodunia.R;

public class MainMatkulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        Button btnTambahMatkul = (Button)findViewById(R.id.btnTambahMatkul);
        Button btnUbahMatkul = (Button)findViewById(R.id.btnUbahMatkul);
        Button btnLihatMatkul = (Button)findViewById(R.id.btnLihatMatkul);
        Button btnHapusMatkul = (Button)findViewById(R.id.btnHapusMatkul);

        //action
        btnLihatMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMatkulActivity.this, DaftarMatkulActivity.class);
                startActivity(i);
            }
        });
        btnTambahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMatkulActivity.this, TambahMatkulActivity.class);
                startActivity(i);
            }
        });
        btnHapusMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMatkulActivity.this, HapusMatkulActivity.class);
                startActivity(i);
            }
        });
        btnUbahMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMatkulActivity.this, UbahMatkulActivity.class);
                startActivity(i);
            }
        });
    }
}