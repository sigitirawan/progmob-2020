package com.example.halodunia.CrudMatakuliah;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.halodunia.Adapter.MatkulCRUDRecyclerAdapter;
import com.example.halodunia.Model.Matakuliah;
import com.example.halodunia.Network.GetDataService;
import com.example.halodunia.Network.RetrofitClientInstance;
import com.example.halodunia.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarMatkulActivity extends AppCompatActivity {
    RecyclerView rvMatkul;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<Matakuliah> matakuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_matkul);
        rvMatkul = (RecyclerView)findViewById(R.id.rvMatkul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Tunggu");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Matakuliah>> call = service.getMatakuliah("72180212");

        call.enqueue(new Callback<List<Matakuliah>>() {
            @Override
            public void onResponse(Call<List<Matakuliah>> call, Response<List<Matakuliah>> response) {
                pd.dismiss();
                matakuliahList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matakuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DaftarMatkulActivity.this);
                rvMatkul.setLayoutManager(layoutManager);
                rvMatkul.setAdapter(matkulAdapter);

            }

            @Override
            public void onFailure(Call<List<Matakuliah>> call, Throwable t) {
                Toast.makeText(DaftarMatkulActivity.this,"ERROR", Toast.LENGTH_LONG);
            }
        });
    }
}